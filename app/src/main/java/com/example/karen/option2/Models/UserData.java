package com.example.karen.option2.Models;

/**
 * Created by Karen on 07.02.2018.
 */

public class UserData {

    private String cod;
    private Double calctime;
    private Integer cnt;
    private java.util.List<com.example.karen.option2.Models.List> list = null;



    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getCalctime() {
        return calctime;
    }

    public void setCalctime(Double calctime) {
        this.calctime = calctime;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }
    public java.util.List<com.example.karen.option2.Models.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.example.karen.option2.Models.List> list) {
        this.list = list;
    }

}
