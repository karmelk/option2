package com.example.karen.option2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.karen.option2.Interface.RetrofitAPI;
import com.example.karen.option2.Models.UserData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private final String baseURL="http://samples.openweathermap.org/data/2.5/box/";
    private ArrayAdapter<String> adapter;
    Spinner spinner;
    TextView temp,pressure,humidity,speed,description ;
    List<com.example.karen.option2.Models.List> userDataList = new ArrayList<>();
    List<String> cityName = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        temp =(TextView) findViewById(R.id.temp) ;
        pressure =(TextView) findViewById(R.id.pressure) ;
        humidity =(TextView) findViewById(R.id.humidity) ;
        speed =(TextView) findViewById(R.id.speed) ;
        description =(TextView) findViewById(R.id.description) ;

        getRetrofitObject();
        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cityName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                temp.setText("Temperature: "+String.valueOf(userDataList.get(selectedItemPosition).getMain().getTemp()));
                pressure.setText("Pressure: "+String.valueOf(userDataList.get(selectedItemPosition).getMain().getPressure()));
                humidity.setText("Humidity: "+String.valueOf(userDataList.get(selectedItemPosition).getMain().getHumidity()));
                speed.setText("Wind speed: "+String.valueOf(userDataList.get(selectedItemPosition).getWind().getSpeed()));
                description.setText("Description: "+userDataList.get(selectedItemPosition).getWeather().get(0).getDescription());
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getRetrofitObject() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitAPI service = retrofit.create(RetrofitAPI.class);
        Call<UserData> call = service.getManagers();
        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                if (response.isSuccessful()) {
                    int recode = response.code();
                    UserData postManResponse = response.body();
                    for (int i = 0; i < postManResponse.getList().size(); i++) {
                        userDataList.add(postManResponse.getList().get(i));
                        cityName.add(postManResponse.getList().get(i).getName());
                    }
                    spinner.setAdapter(adapter);
                }else {
                    int errorkey = response.code();
                    Toast.makeText(MainActivity.this, "You will find error with this key "+
                            errorkey, Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
