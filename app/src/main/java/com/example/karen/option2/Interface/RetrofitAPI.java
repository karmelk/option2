package com.example.karen.option2.Interface;

import com.example.karen.option2.Models.UserData;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Karen on 03.11.2017.
 */

public interface RetrofitAPI {

    @GET("city?bbox=12,32,15,37,10&appid=0cebb604307d09b25d2c3367fc9a5e9c")
    Call <UserData> getManagers();

}