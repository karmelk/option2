package com.example.karen.option2.Models;

/**
 * Created by Karen on 07.02.2018.
 */

public class Wind {

    private double speed;

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

}
